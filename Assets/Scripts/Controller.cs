﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Controller : MonoBehaviour
{
    //GameObjects
    public GameObject board;
    public GameObject[] cops = new GameObject[2];
    public GameObject robber;
    public Text rounds;
    public Text finalMessage;
    public Button playAgainButton;

    //Otras variables
    public Tile[] tiles = new Tile[Constants.NumTiles];
    private int roundCount = 0;
    private int state;
    private int clickedTile = -1;
    private int clickedCop = 0;


    //Diccionario para almacenar la distancia desde los vertices del ladron hasta los policias (magnitud absoluta)
    public Dictionary<int, int> tileDistance = new Dictionary<int, int>();

                    
    void Start()
    {        
        InitTiles();
        InitAdjacencyLists();
        state = Constants.Init;
    }
        
    //Rellenamos el array de casillas y posicionamos las fichas
    void InitTiles()
    {
        for (int fil = 0; fil < Constants.TilesPerRow; fil++)
        {
            GameObject rowchild = board.transform.GetChild(fil).gameObject;            

            for (int col = 0; col < Constants.TilesPerRow; col++)
            {
                GameObject tilechild = rowchild.transform.GetChild(col).gameObject;                
                tiles[fil * Constants.TilesPerRow + col] = tilechild.GetComponent<Tile>();                         
            }
        }
                
        cops[0].GetComponent<CopMove>().currentTile=Constants.InitialCop0;
        cops[1].GetComponent<CopMove>().currentTile=Constants.InitialCop1;
        robber.GetComponent<RobberMove>().currentTile=Constants.InitialRobber;           
    }

    public void InitAdjacencyLists()
    {
        //Matriz de adyacencia
        int[,] matriu = new int[Constants.NumTiles, Constants.NumTiles];

        //TODO: Inicializar matriz a 0's
        for(int row=0; row<matriu.GetLength(0);row++){
            for(int column=0; column<matriu.GetLength(1); column++){
                //Debug.Log("position: ["+row+"]["+column+"]");
                matriu[row,column] = 0;
            }

        }


        //TODO: Para cada posición, rellenar con 1's las casillas adyacentes (arriba, abajo, izquierda y derecha)
        for(int i=0; i<matriu.GetLength(1); i++){
            
            //Posición adyacente a la DERECHA, el modulo de la posición + 1 entre el número de casillas por fila, siempre sera distinto de 0
            // Salvo en la posición más a la derecha de cada fila
            if ((i + 1) % Constants.TilesPerRow != 0) {
                 matriu[i, i + 1] = 1; //Se le añade la posición + 1
            }
            //Posición adyacente a la IZQUIERDA, igual que el anterior pero sin añadirle 1 a la posición.
            //Siempre será distint de 0, salvo en la casilla más a la izquierda de cada fila
            if ((i) % Constants.TilesPerRow != 0) {
                matriu[i, i - 1] = 1; //Se le añade la posición +1
            }
            //Posición de adyacencia SUPERIOR, siempre que la posición actual no sea mayor a las posiciones de la última fila
            if(i < (Constants.NumTiles - Constants.TilesPerRow)){
                matriu[i, i + Constants.TilesPerRow] = 1; //Se añade la posición equivalente en la siguiente fila (+8)
            }
            //Posición adyacente INFERIOR, siempre que no sea una posición de la 1ª fila
            if(i>=Constants.TilesPerRow){
                matriu[i, i-Constants.TilesPerRow]=1; //Se añade la posición de la fila inferior (-8)
            }
            

        }

        //TODO: Rellenar la lista "adjacency" de cada casilla con los índices de sus casillas adyacentes
        for(int i=0; i<matriu.GetLength(0);i++){
            for(int j=0; j<matriu.GetLength(1);j++){
                if(matriu[i,j]==1){ tiles[i].GetComponent<Tile>().adjacency.Add(j); } //Añadimos a la tile en la posición i, el indice (j)
            }
        }


    }

    //Reseteamos cada casilla: color, padre, distancia y visitada
    public void ResetTiles()
    {        
        foreach (Tile tile in tiles)
        {
            tile.Reset();
        }
    }

    public void ClickOnCop(int cop_id)
    {
        switch (state)
        {
            case Constants.Init:
            case Constants.CopSelected:                
                clickedCop = cop_id;
                clickedTile = cops[cop_id].GetComponent<CopMove>().currentTile;
                tiles[clickedTile].current = true;

                ResetTiles();
                FindSelectableTiles(true);

                state = Constants.CopSelected;                
                break;            
        }
    }

    public void ClickOnTile(int t)
    {                     
        clickedTile = t;

        switch (state)
        {            
            case Constants.CopSelected:
              //Si es una casilla roja, nos movemos
                if (tiles[clickedTile].selectable)
                {
                   cops[clickedCop].GetComponent<CopMove>().MoveToTile(tiles[clickedTile]);
                   cops[clickedCop].GetComponent<CopMove>().currentTile=tiles[clickedTile].numTile;
                   tiles[clickedTile].current = true;   
                    
                    state = Constants.TileSelected;
                }

                break;
            case Constants.TileSelected:
                state = Constants.Init;
                break;
            case Constants.RobberTurn:
                state = Constants.Init;
                break;
        }
    }

    public void FinishTurn()
    {
        switch (state)
        {            
            case Constants.TileSelected:
                ResetTiles();

                state = Constants.RobberTurn;
                RobberTurn();
                break;
            case Constants.RobberTurn:                
                ResetTiles();
                IncreaseRoundCount();
                if (roundCount <= Constants.MaxRounds)
                    state = Constants.Init;
                else
                    EndGame(false);
                break;
        }

    }

    public void RobberTurn()
    {
        clickedTile = robber.GetComponent<RobberMove>().currentTile;
        tiles[clickedTile].current = true;
        FindSelectableTiles(false);


        int furthestTile = robber.GetComponent<RobberMove>().currentTile;
        int maxDistance = 0;
        foreach (KeyValuePair<int, int> tile in tileDistance) {
            if (tile.Value > maxDistance) { 
                maxDistance = tile.Value;
            }

            //Debug.Log("Casilla " + tile.Key + " distancia: " + tile.Value);
        }

        //Guardamos todas las casillas posibles con la mayor distancia obtenidas y elegimos una aleatoria
        List<int> robberPossibleTiles = new List<int>();
        foreach (KeyValuePair<int, int> tile in tileDistance){
            if(tile.Value == maxDistance) { robberPossibleTiles.Add(tile.Key); }
        }

        furthestTile = robberPossibleTiles[Random.Range(0, robberPossibleTiles.Count)];
        robber.GetComponent<RobberMove>().MoveToTile(tiles[furthestTile]);
        robber.GetComponent<RobberMove>().currentTile = furthestTile;

        tileDistance.Clear();


    }

    public void EndGame(bool end)
    {
        if(end)
            finalMessage.text = "You Win!";
        else
            finalMessage.text = "You Lose!";
        playAgainButton.interactable = true;
        state = Constants.End;
    }

    public void PlayAgain()
    {
        cops[0].GetComponent<CopMove>().Restart(tiles[Constants.InitialCop0]);
        cops[1].GetComponent<CopMove>().Restart(tiles[Constants.InitialCop1]);
        robber.GetComponent<RobberMove>().Restart(tiles[Constants.InitialRobber]);
                
        ResetTiles();

        playAgainButton.interactable = false;
        finalMessage.text = "";
        roundCount = 0;
        rounds.text = "Rounds: ";

        state = Constants.Restarting;
    }

    public void InitGame()
    {
        state = Constants.Init;
         
    }

    public void IncreaseRoundCount()
    {
        roundCount++;
        rounds.text = "Rounds: " + roundCount;
    }

    public void FindSelectableTiles(bool cop)
    {
                 
        int indexcurrentTile;        

        if (cop==true)
            indexcurrentTile = cops[clickedCop].GetComponent<CopMove>().currentTile;
        else
            indexcurrentTile = robber.GetComponent<RobberMove>().currentTile;

        //La ponemos rosa porque acabamos de hacer un reset
        tiles[indexcurrentTile].current = true;

        //Cola para el BFS
        Queue<Tile> nodes = new Queue<Tile>();


        //TODO: Implementar BFS. Los nodos seleccionables los ponemos como selectable=true
        //Tendrás que cambiar este código por el BFS
        
       Tile node;
        int maxDistance = 2; //Rango de movimiento maximo

        //Se mete en la cola del BFS el 1er Nodo (el clikado)
        nodes.Enqueue(tiles[clickedTile]);
        List<int>reachableNodes = new List<int>(); //Listado de nodos alcanzables
        
        while(nodes.Count!=0){
            //Se recorren todos los nodos ahsta que se sacan de la cola
            node = nodes.Dequeue(); //Se extrae el nodo
            if((node.distance + 1) <= maxDistance ){
                foreach(int pos in node.adjacency){
                    //if node addjacency includes other cop...
                    bool isWalkable = true;
                    foreach(GameObject _cop in cops){
                        int occupedTile = _cop.GetComponent<CopMove>().currentTile;
                        if(occupedTile == pos) { isWalkable = false;  }
                    }

                    if (isWalkable) { 

                        if (!tiles[pos].visited && !tiles[pos].current){ //Si no se ha visitado todavía este nodo... se mete en la cola (ni es el actual)
                            tiles[pos].visited = true;
                            tiles[pos].distance = node.distance+1;
                            tiles[pos].parent = node;

                            nodes.Enqueue(tiles[pos]); reachableNodes.Add(pos);
                            tiles[pos].selectable = true;
                            //Debug.Log("Tile ("+pos+")"+tiles[pos].selectable);


                            //Por cada nodo se busca la distancia hasta los COPS
                            if (!cop) {
                                int cops_qty = 0;
                                foreach(GameObject _cop in cops) {  //Por cada COP
                                    cops_qty++;
                                    Queue<Tile> nodesDistance = new Queue<Tile>();
                                    int copNode = _cop.GetComponent<CopMove>().currentTile; //Posición a buscar


                                    nodesDistance.Enqueue(tiles[pos]);
                                    Tile nodeDistance;
                                    foreach(Tile _tile in tiles) {
                                        _tile.ResetCopPath();
                                    } //Se resetan todas las tiles copiadas

                                    while (nodesDistance.Count != 0)
                                    {
                                        nodeDistance = nodesDistance.Dequeue(); //Saco un nodo (1º el raiz)
                                        foreach (int index in nodeDistance.adjacency) //Se obtienen las adjacencias de dicho nodo
                                        {
                                            if (!tiles[index].visitedCopPath) {
                                                tiles[index].visitedCopPath = true;
                                                tiles[index].distanceCopPath = nodeDistance.distanceCopPath + 1;
                                                tiles[index].parentCopPath = nodeDistance;

                                                nodesDistance.Enqueue(tiles[index]);
                                            }

                                        }

                                    } //Fin bucle BFS ladron


                                    //Calculado el BFS podemos obtener la distancia desde el nodo
                                    if (tileDistance.ContainsKey(pos)) {
                                        int t_distance = (int)((tileDistance[pos] + tiles[copNode].distanceCopPath)/cops_qty); //Distancia maxima media entre los 2 cops
                                        //int t_distance = (tileDistance[pos] + tiles[copNode].distanceCopPath); //Suma de distancias 
                                        tileDistance[pos] =  t_distance;
                                    } else { tileDistance.Add(pos, tiles[copNode].distanceCopPath); }


                                } //Fin del bucle por cada COP

                            } //Fin Busqueda de distancia para el ladron



                        }

                    } //COP IN THE ADJACENCY
                }
            }
            
        }





    }
    
   
    

    

   

       
}
